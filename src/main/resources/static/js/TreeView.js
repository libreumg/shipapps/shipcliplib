class TreeView {
    constructor(options) {
        this.jstreeElement = $(`#${options.treeId}`);
        this.treeFilterElement = $(`#${options.treeFilterId}`);
        this.treePathElement = $(`#${options.treePathId}`);
        this.collapseBtnElement = $(`#${options.collapseBtnId}`);
        this.refreshBtnElement = $(`#${options.refreshBtnId}`);
        this.nodeTextProperty = options.nodeTextProperty;
        this.api = options.backendApiUrl;
        this.nodeIconClass = options.nodeIconClass;
        this.leafIconClass = options.leafIconClass;
      }

      init() {
          this.jstreeElement.jstree({
            core: {
              data: (node, cb) => {
                if (node.id === "#") {
                  $.ajax({
                    url: `${this.api}/children`,
                    dataType: "json", 
                    success: (data) => {
                      cb(data.map(this.createJsTreeObj.bind(this)));
                    },
                    error: (err) => {
                      console.log(err);
                    },
                  });
                } else {
                  $.ajax({
                    url: `${this.api}/children/${node.id}`,
                    dataType: "json",
                    success: (data) => {
                      cb(data.map(this.createJsTreeObj.bind(this)));
                    },
                    error: (err) => {
                      console.log(err);
                    },
                  });
                }
              },
            },
            multiple: false,
            types: {
              group: {
                icon: this.nodeIconClass,
              },
              variable: {
                icon: this.leafIconClass,
              },
            },
            state: { key: "jstree_state" },
            search: {
              ajax: async (nodeId, cb) => {
                const data = await $.ajax({
                  url: `${this.api}/path/${nodeId}`,
                  dataType: "json"
                });
                cb(data);
              },
              search_callback: (id, node) => {
                if (node.id != id) return false;
                return true;
              },
            },
            plugins: [
              "types", 
              "search", 
              "state",
            ],
        });

        this.refreshBtnElement.on("click", () => { 
          this.jstreeElement.jstree(true).refresh();
        });

        this.treeFilterElement.select2({
          ajax: {
            url: params => params.term ? `${this.api}/filter/${params.term}` : null,
            dataType: 'json',
            delay: 250,
            processResults: (data) => {
              return {
                results: data.map((variable) => {
                  return {
                    id: variable.keyElement,
                    text: variable.uniqueName,
                  }
                })
              };
            },
          },
          theme: 'bootstrap-5',
        });

        this.treeFilterElement.on('select2:select', async (e) => {
          this.jstreeElement.jstree(true).deselect_all();
          this.jstreeElement.jstree(true).search(e.params.data.id);
        });
        this.jstreeElement.on('search.jstree', (e, data) => {
          this.jstreeElement.jstree(true).select_node(data.nodes);
          console.log("jump to node with id: " + data.nodes[0].id);
          location.href = "#" + data.nodes[0].id;
        });


        this.jstreeElement.one("ready.jstree", () => {
          if (this.isMultiselect === true) return
          this.jstreeElement.jstree(true).settings.core.multiple = this.isMultiselect;
        });

        this.collapseBtnElement.on("click", () => {
          this.jstreeElement.jstree(true).deselect_all();
          this.jstreeElement.jstree(true).close_all();
        });

        this.jstreeElement.on("changed.jstree", (e, data) => {
          const path = data.selected.length ? "/" + this.jstreeElement.jstree(true).get_path(data.selected, "/") : "/";
          this.treePathElement.text(path);
        });
        this.jstreeElement.on("redraw.jstree", this.createTooltips);
        this.jstreeElement.on("after_open.jstree", this.createTooltips);
        this.jstreeElement.on("after_close.jstree", this.createTooltips);
        this.jstreeElement.on("dehover_node.jstree", (e, eventobjects)=>{
          $("#" + eventobjects.node.id + "_anchor").tooltip("hide");
        });
      }
    createTooltips(){        
      $("#jstree *").tooltip({"placement": "right"});
    }
    /**
     * Creates and returns jsTree readable node object
     * @param node the JsonChild or JsonProperties object to create the object from.
     * 
     * @returns jsTree readable node object
     */
    createJsTreeObj(node) {
      return {
        id: node.keyElement,
        parentId: node.keyParentElement,
        text: node[this.nodeTextProperty],
        state: {
          opened: false,
          selected: false,
        },
        children: node.hasChilds,
        original: node,
        properties: undefined,
        type: node.hasChilds === true ? "group" : "variable",
        a_attr: {
          "data-toggle": "tooltip",
          title: node.uniqueName
        }
      } 
  }
}