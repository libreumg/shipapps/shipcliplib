package de.ship.clip.lib.profile;

import org.jooq.DSLContext;

import de.ship.clip.lib.common.ProfileHelper;
import de.ship.clip.lib.profile.model.ProfileBean;

/**
 * 
 * @author henkej
 * 
 */
public abstract class ProfileService {
	/**
	 * toggles the profile in the database
	 * 
	 * @param repository the repository
	 * @param jooq       the jooq context
	 * @param keycloakID the keycloak ID
	 */
	public void toggleProfile(ProfileRepository repository, DSLContext jooq, String keycloakID) {
		ProfileBean bean = repository.getProfile(jooq, keycloakID);
		switch (bean.getTheme()) {
		case ProfileHelper.PROFILE_THEME_DARK:
			bean.setTheme(ProfileHelper.PROFILE_THEME_LIGHT);
			break;
		case ProfileHelper.PROFILE_THEME_LIGHT:
			bean.setTheme(ProfileHelper.PROFILE_THEME_DARK);
			break;
		default:
		}
		repository.updateTheme(jooq, keycloakID, bean.getTheme());
	}
}
