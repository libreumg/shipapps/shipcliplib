package de.ship.clip.lib.profile;

import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * 
 * @author henkej
 * 
 */
public abstract class ProfileRestRepository {
	private final static Logger LOGGER = LogManager.getLogger(ProfileRestRepository.class);

	/**
	 * loads the theme from the url
	 * 
	 * @param url the url
	 * @return the theme
	 */
	public String loadThemeFromUrl(String url) {
		if (url == null) {
			throw new NullPointerException("url is null; cannot load theme from nowhere");
		}
		LOGGER.debug("try to load from url: {}", url);
		RestTemplate rest = new RestTemplate();
		rest.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		return rest.getForObject(url, String.class);
	}
}
