package de.ship.clip.lib.profile.model;

import java.io.Serializable;

/**
 * 
 * @author henkej
 * 
 */
public class ProfileBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String keycloakID;
	private String theme;

	public static final ProfileBean of(String keycloakID) {
		ProfileBean bean = new ProfileBean();
		bean.setKeycloakID(keycloakID);
		return bean;
	}
	
	/**
	 * @return the keycloakID
	 */
	public String getKeycloakID() {
		return keycloakID;
	}

	/**
	 * @param keycloakID the keycloakID to set
	 */
	public void setKeycloakID(String keycloakID) {
		this.keycloakID = keycloakID;
	}

	/**
	 * @return the theme
	 */
	public String getTheme() {
		return theme;
	}

	/**
	 * @param theme the theme to set
	 */
	public void setTheme(String theme) {
		this.theme = theme;
	}

}
