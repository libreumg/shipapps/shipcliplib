package de.ship.clip.lib.profile;

import static de.ship.clip.db.Tables.T_PROFILE;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.InsertValuesStep1;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;

import de.ship.clip.db.tables.records.TProfileRecord;
import de.ship.clip.lib.common.ProfileHelper;
import de.ship.clip.lib.profile.model.ProfileBean;

/**
 * 
 * @author henkej
 * 
 */
public abstract class ProfileRepository {
	private static final Logger LOGGER = LogManager.getLogger(ProfileRepository.class);

	/**
	 * update the theme in the database
	 * 
	 * @param jooq       the jooq context
	 * @param keycloakID the ID of the person
	 * @param theme      the theme
	 * @return the number of affected database rows; should be 1 (or 0 if keycloakID
	 *         does not exist)
	 */
	public int updateTheme(DSLContext jooq, String keycloakID, String theme) {
		UpdateConditionStep<TProfileRecord> sql = jooq
		// @formatter:off
			.update(T_PROFILE)
			.set(T_PROFILE.THEME, theme)
			.where(T_PROFILE.KEYCLOAK_ID.eq(keycloakID));
		// @formatter:on
		LOGGER.trace(sql.toString());
		return sql.execute();
	}

	/**
	 * get the profile from the database; if not available yet, register one
	 * 
	 * @param jooq       the jooq context
	 * @param keycloakID the ID from keycloak
	 * @return the profile bean or null, if keycloakID is null
	 */
	public ProfileBean getProfile(DSLContext jooq, String keycloakID) {
		if (keycloakID == null) {
			return null;
		} else {
			SelectConditionStep<TProfileRecord> sql = jooq
			// @formatter:off
				.selectFrom(T_PROFILE)
				.where(T_PROFILE.KEYCLOAK_ID.eq(keycloakID));
			// @formatter:on
			LOGGER.trace(sql.toString());
			ProfileBean bean = ProfileBean.of(keycloakID);
			List<TProfileRecord> fetched = sql.fetch();
			if (fetched.size() < 1) {
				InsertValuesStep1<TProfileRecord, String> sql1 = jooq
				// @formatter:off
					.insertInto(T_PROFILE, T_PROFILE.KEYCLOAK_ID)
					.values(keycloakID);
				// @formatter:on
				LOGGER.trace(sql.toString());
				sql1.execute();
				bean.setTheme(ProfileHelper.PROFILE_THEME_LIGHT); // default value for the theme
			}
			for (TProfileRecord r : fetched) {
				bean.setTheme(r.getTheme());
			}
			return bean;
		}
	}
}
