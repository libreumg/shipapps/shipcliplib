package de.ship.clip.lib.common;

import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

/**
 * 
 * @author henkej
 *
 */
public class ConfigurationHelper {
	
	/**
	 * the string for the message sources basenames; add it before the local one to overwrite keys on demand
	 */
	public static final String LIB_MESSAGES = "classpath:/shipcliplib/messages";
	
	/**
	 * get the locale change interceptor
	 * 
	 * @return the locale change interceptor
	 */
	public static final LocaleChangeInterceptor getLocaleChangeInterceptor() {
		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
		localeChangeInterceptor.setParamName("lang");
		return localeChangeInterceptor;
	}
}
