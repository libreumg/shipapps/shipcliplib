package de.ship.clip.lib.common;

import org.springframework.boot.info.BuildProperties;

/**
 * 
 * @author henkej
 *
 */
public abstract class BuildInformationProvider {

	/**
	 * get the application version
	 * 
	 * @return the application version
	 */
	public abstract String getVersion();

	/**
	 * get the context path
	 * 
	 * @return the context path
	 */
	public abstract String getContextPath();

	/**
	 * get version from build properties
	 * 
	 * @param buildProperties the build properties
	 * @return the version or ?
	 */
	public final String getVersion(BuildProperties buildProperties) {
		return buildProperties != null ? buildProperties.getVersion() : "?";
	}

	/**
	 * get the context path
	 * 
	 * @param servletContextPath the context path
	 * @return the corrected context path
	 */
	public final String getContextPath(String servletContextPath) {
		return (servletContextPath != null && !servletContextPath.isBlank()) ? String.format("/%s/", servletContextPath)
				: "/";
	}
}
