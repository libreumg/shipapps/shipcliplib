package de.ship.clip.lib.common;

/**
 * 
 * @author henkej
 *
 */
public abstract class KeycloakInformationProvider {

	/**
	 * get the keycloak user URL
	 * 
	 * @return the URL
	 */
	public abstract String getKeycloakUserUrl();

	/**
	 * get the url of the user client
	 * 
	 * @param issuerUri the issuer uri, e.g. https://keycloak.ship-med.uni-greifswald.de/realms/ship
	 *
	 * @return the url of the user client
	 */
	public final String getKeycloakUserUrl(String issuerUri) {
		return String.format("%s/account", issuerUri);
	}

	/**
	 * get the url of the user client
	 * 
	 * @param url the url, e.g. https://keycloak.ship-med.uni-greifswald.de/realms/ship/protocol/openid-connect/auth
	 * @param realm the realm, e.g. ship
	 *
	 * @return the url of the user client
	 */
	public final String getKeycloakLogoutUrl(String url, String realm) {
		String final_url = url == null ? null : (url.substring(0, url.length() - (url.endsWith("/") ? 1 : 0)));
		return String.format("%s/realms/%s/protocol/openid-connect/logout?redirect_uri=encodedRedirectUri", final_url, realm);
	}
}
