package de.ship.clip.lib.common;
/**
 * 
 * @author henkej
 * 
 */
public class ProfileHelper {
	/**
	 * name of the profile bean in the thymeleaf context
	 */
	public static final String PROFILE_BEAN_NAME = "profile";
	
	/**
	 * GET endpoint to toggle the profile theme
	 */
	public static final String PROFILE_TOGGLE_THEME = "/toggletheme";
	
	/**
	 * the profile theme name for dark
	 */
	public static final String PROFILE_THEME_DARK = "dark";
	
	/**
	 * the profile theme name for light
	 */
	public static final String PROFILE_THEME_LIGHT = "light";
}
