package de.ship.clip.lib.menu;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class DisplayLabel implements Serializable {
	private static final long serialVersionUID = 1L;

	private String langkey;
	private String name;

	private DisplayLabel() {
	}

	/**
	 * create a new display label with a translation key
	 * 
	 * @param langkey the key of the translation
	 * @return the bean
	 */
	public static final DisplayLabel ofTranslation(String langkey) {
		DisplayLabel bean = new DisplayLabel();
		bean.setLangkey(langkey);
		return langkey == null ? null : bean;
	}

	/**
	 * create a new display label with a name that will be used directly
	 * 
	 * @param name the name
	 * @return the bean
	 */
	public static final DisplayLabel ofName(String name) {
		DisplayLabel bean = new DisplayLabel();
		bean.setName(name);
		return name == null ? null : bean;
	}

	/**
	 * @return true if langkey is not null and not empty
	 */
	public Boolean hasTranslation() {
		return langkey != null && !langkey.isBlank();
	}

	/**
	 * @return the langkey
	 */
	public String getLangkey() {
		return langkey;
	}

	/**
	 * @param langkey the langkey to set
	 */
	public void setLangkey(String langkey) {
		this.langkey = langkey;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
