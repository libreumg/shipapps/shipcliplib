package de.ship.clip.lib.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class ListOfMenuitemBeansWrapper implements Serializable {
	private static final long serialVersionUID = 4L;

	private List<MenuitemBean> list;
	private final Long version;

	private ListOfMenuitemBeansWrapper() {
		this.version = serialVersionUID;
		this.list = new ArrayList<>();
	}

	/**
	 * generate a new wrapper with the list
	 * 
	 * @param list the list
	 * @return the wrapper
	 */
	public static final ListOfMenuitemBeansWrapper of(List<MenuitemBean> list) {
		ListOfMenuitemBeansWrapper bean = new ListOfMenuitemBeansWrapper();
		bean.getList().addAll(list);
		return bean;
	}

	/**
	 * @return the list
	 */
	public List<MenuitemBean> getList() {
		return list;
	}

	/**
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}
}
