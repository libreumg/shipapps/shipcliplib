package de.ship.clip.lib.menu;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import com.google.gson.Gson;

/**
 * 
 * @author henkej
 *
 */
public class ListOfMenuitemBeansWrapperConverter implements HttpMessageConverter<ListOfMenuitemBeansWrapper> {

	@Override
	public boolean canRead(Class<?> clazz, MediaType mediaType) {
		return clazz.equals(MenuitemBean.class) && mediaType.equals(MediaType.APPLICATION_JSON);
	}

	@Override
	public boolean canWrite(Class<?> clazz, MediaType mediaType) {
		return clazz.equals(MenuitemBean.class) && mediaType.equals(MediaType.APPLICATION_JSON);
	}

	@Override
	public List<MediaType> getSupportedMediaTypes() {
		return Arrays.asList(MediaType.APPLICATION_JSON);
	}

	@Override
	public ListOfMenuitemBeansWrapper read(Class<? extends ListOfMenuitemBeansWrapper> clazz, HttpInputMessage inputMessage)
			throws IOException, HttpMessageNotReadableException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		inputMessage.getBody().transferTo(out);
		return new Gson().fromJson(out.toString(), ListOfMenuitemBeansWrapper.class);
	}

	@Override
	public void write(ListOfMenuitemBeansWrapper bean, MediaType contentType, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {
		outputMessage.getHeaders().setContentType(contentType);
		String json = new Gson().toJson(bean);
		outputMessage.getBody().write(json.getBytes());
	}
}
