package de.ship.clip.lib.menu;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class MenuitemBean implements Serializable {
	private static final long serialVersionUID = 3L;

	public static final String SELECTED_CLASS = "selectedmenuitem";
	
	private String appId;
	private String url;
	private String iconUrl;
	private DisplayLabel label;
	private DisplayLabel description;
	private String cssClass;
	private String keycloakRole;
	
	private MenuitemBean() {
	}

	/**
	 * generates a new bean with the corresponding app ID
	 * 
	 * @param appId the app ID
	 * @return the generated bean
	 */
	public static final MenuitemBean of(String appId) {
		MenuitemBean bean = new MenuitemBean();
		bean.setAppId(appId);
		return bean;
	}

	/**
	 * add cssClass and return this bean
	 * 
	 * @param cssClass the css class
	 * @return the css class
	 */
	public MenuitemBean withCssClass(String cssClass) {
		this.cssClass = cssClass;
		return this;
	}
	
	/**
	 * add label and return this bean
	 * 
	 * @param label the label
	 * @return this bean
	 */
	public MenuitemBean withLabel(DisplayLabel label) {
		this.label = label;
		return this;
	}

	/**
	 * add description and return this bean
	 * 
	 * @param description the description
	 * @return this bean
	 */
	public MenuitemBean withDescription(DisplayLabel description) {
		this.description = description;
		return this;
	}

	/**
	 * add url and return the bean
	 * 
	 * @param url the url; should be an absolute path
	 * @return this bean
	 */
	public MenuitemBean withUrl(String url) {
		this.url = url;
		return this;
	}

	/**
	 * add the icon url and return the bean
	 * 
	 * @param iconUrl the icon url; should be an absolute path
	 * @return this bean
	 */
	public MenuitemBean withIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
		return this;
	}
	
	/**
	 * add expected keycloak role that must fit to display the menu item
	 * 
	 * @param keycloakRole the keycloak role if any
	 * @return this bean
	 */
	public MenuitemBean withRole(String keycloakRole) {
		this.keycloakRole = keycloakRole;
		return this;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the label
	 */
	public DisplayLabel getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(DisplayLabel label) {
		this.label = label;
	}

	/**
	 * @return the description
	 */
	public DisplayLabel getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(DisplayLabel description) {
		this.description = description;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the iconUrl
	 */
	public String getIconUrl() {
		return iconUrl;
	}

	/**
	 * @param iconUrl the iconUrl to set
	 */
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	/**
	 * @return the cssClass
	 */
	public String getCssClass() {
		return cssClass;
	}

	/**
	 * @param cssClass the cssClass to set
	 */
	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	/**
	 * @return the keycloakRole
	 */
	public String getKeycloakRole() {
		return keycloakRole;
	}

	/**
	 * @param keycloakRole the keycloakRole to set
	 */
	public void setKeycloakRole(String keycloakRole) {
		this.keycloakRole = keycloakRole;
	}
}
