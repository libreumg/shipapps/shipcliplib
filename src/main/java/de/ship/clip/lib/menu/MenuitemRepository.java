package de.ship.clip.lib.menu;

import static de.ship.clip.db.Tables.T_DISPLAYLABEL;
import static de.ship.clip.db.Tables.T_MENUITEM;
import static de.ship.clip.db.Tables.T_TRANSLATION;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record10;
import org.jooq.SelectSeekStep1;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import de.ship.clip.db.enums.EnumLocale;
import de.ship.clip.db.tables.TDisplaylabel;
import de.ship.clip.db.tables.TTranslation;

/**
 * 
 * @author henkej
 *
 */
public abstract class MenuitemRepository {
	private final static Logger LOGGER = LogManager.getLogger(MenuitemRepository.class);

	private DisplayLabel getFirstNotNullValue(DisplayLabel... values) {
		for (DisplayLabel d : values) {
			if (d != null) {
				return d;
			}
		}
		return null;
	}

	/**
	 * get all menu items from the database
	 * 
	 * @param jooq   the DSL context of jooq
	 * @param locale the current locale to find the correct translation if available
	 * 
	 * @return the menu items
	 */
	public List<MenuitemBean> getMenuitems(DSLContext jooq, Locale locale) {
		TDisplaylabel LABEL = T_DISPLAYLABEL.as("label");
		TDisplaylabel DESCRIPTION = T_DISPLAYLABEL.as("description");
		TTranslation LABEL_TRANS = T_TRANSLATION.as("label_trans");
		TTranslation DESCRIPTION_TRANS = T_TRANSLATION.as("description_trans");
		SelectSeekStep1<Record10<String, String, String, String, String, String, String, String, String, String>, Integer> sql = jooq
		// @formatter:off
			.select(T_MENUITEM.APP_ID, T_MENUITEM.URL, T_MENUITEM.ICON_URL, T_MENUITEM.KEYCLOAK_ROLE, LABEL.LABEL, LABEL.LANGKEY, LABEL_TRANS.VALUE, 
					    DESCRIPTION.LABEL, DESCRIPTION.LANGKEY, DESCRIPTION_TRANS.VALUE)
			.from(T_MENUITEM)
			.leftJoin(LABEL).on(LABEL.PK.eq(T_MENUITEM.FK_LABEL))
			.leftJoin(DESCRIPTION).on(DESCRIPTION.PK.eq(T_MENUITEM.FK_DESCRIPTION))
			.leftJoin(LABEL_TRANS).on(LABEL_TRANS.LANGKEY.eq(LABEL.LANGKEY).and(LABEL_TRANS.LOCALECODE.eq(EnumLocale.lookupLiteral(locale.toString()))))
			.leftJoin(DESCRIPTION_TRANS).on(DESCRIPTION_TRANS.LANGKEY.eq(DESCRIPTION.LANGKEY).and(DESCRIPTION_TRANS.LOCALECODE.eq(EnumLocale.lookupLiteral(locale.toString()))))
			.orderBy(T_MENUITEM.ORDER_NR);
		// @formatter:on
		LOGGER.trace(sql.toString());
		List<MenuitemBean> list = new ArrayList<>();
		for (Record10<String, String, String, String, String, String, String, String, String, String> r : sql.fetch()) {
			String appId = r.get(T_MENUITEM.APP_ID);
			String url = r.get(T_MENUITEM.URL);
			String iconUrl = r.get(T_MENUITEM.ICON_URL);
			String keycloakRole = r.get(T_MENUITEM.KEYCLOAK_ROLE);
			String labelName = r.get(LABEL.LABEL);
			String labelLangkey = r.get(LABEL.LANGKEY);
			String labelTranslation = r.get(LABEL_TRANS.VALUE);
			String descrName = r.get(DESCRIPTION.LABEL);
			String descrLangkey = r.get(DESCRIPTION.LANGKEY);
			String descriptionTranslation = r.get(DESCRIPTION_TRANS.VALUE);
			DisplayLabel label = getFirstNotNullValue(DisplayLabel.ofName(labelTranslation), DisplayLabel.ofName(labelName),
					DisplayLabel.ofTranslation(labelLangkey));
			DisplayLabel description = getFirstNotNullValue(DisplayLabel.ofName(descriptionTranslation),
					DisplayLabel.ofName(descrName), DisplayLabel.ofTranslation(descrLangkey));
			list.add(MenuitemBean.of(appId).withUrl(url).withIconUrl(iconUrl).withLabel(label).withDescription(description).withRole(keycloakRole));
		}
		return list;
	}

	/**
	 * load the menu from the url
	 * 
	 * @param url the deep link of the menu
	 * @return the wrapper that contains the list
	 * 
	 * @deprecated use loadMenuFromUrl(url, appid) instead
	 */
	@Deprecated
	public ListOfMenuitemBeansWrapper loadMenuFromUrl(String url) {
		return loadMenuFromUrl(url, "");
	}

	/**
	 * load the menu from the url
	 * 
	 * @param url   the deep link of the menu
	 * @param appId the ID of the current app to mark the corresponding menu entry
	 * @return the wrapper that contains the list
	 */
	public ListOfMenuitemBeansWrapper loadMenuFromUrl(String url, String appId) {
		if (url == null) {
			throw new NullPointerException("url is null; cannot load menu from nowhere");
		}
		LOGGER.debug("try to load from url: {}", url);
		RestTemplate rest = new RestTemplate();
		rest.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		rest.getMessageConverters().add(new ListOfMenuitemBeansWrapperConverter());
		ListOfMenuitemBeansWrapper wrapper = rest.getForObject(url, ListOfMenuitemBeansWrapper.class);
		for (MenuitemBean bean : wrapper.getList()) {
			if (appId != null && bean.getAppId().equals(appId.replace("/", ""))) {
				bean.setCssClass(MenuitemBean.SELECTED_CLASS);
			}
		}
		return wrapper;
	}
}
