package de.ship.clip.lib.tree;

import java.io.Serializable;

/**
 * 
 * @author palmstedtj
 *
 */
public class ElementBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer keyElement;
	private Integer keyParentElement;
	private String shortName;
	private String uniqueName;
	private Boolean hasChilds;

	public static final ElementBean of(Integer keyElement, Integer keyParentElement, String shortName, String uniqueName,
			Boolean hasChilds) {
		ElementBean bean = new ElementBean();
		bean.setKeyElement(keyElement);
		bean.setKeyParentElement(keyParentElement);
		bean.setShortName(shortName);
		bean.setHasChilds(hasChilds);
		bean.setUniqueName(uniqueName);
		return bean;
	}

	/**
	 * @return the keyElement
	 */
	public Integer getKeyElement() {
		return keyElement;
	}

	/**
	 * @param keyElement the keyElement to set
	 */
	public void setKeyElement(Integer keyElement) {
		this.keyElement = keyElement;
	}

	/**
	 * @return the keyParentElement
	 */
	public Integer getKeyParentElement() {
		return keyParentElement;
	}

	/**
	 * @param keyParentElement the keyParentElement to set
	 */
	public void setKeyParentElement(Integer keyParentElement) {
		this.keyParentElement = keyParentElement;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the hasChilds
	 */
	public Boolean getHasChilds() {
		return hasChilds;
	}

	/**
	 * @param hasChilds the hasChilds to set
	 */
	public void setHasChilds(Boolean hasChilds) {
		this.hasChilds = hasChilds;
	}

	/**
	 * @return the uniqueName
	 */
	public String getUniqueName() {
		return uniqueName;
	}

	/**
	 * @param uniqueName the uniqueName to set
	 */
	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}
}
