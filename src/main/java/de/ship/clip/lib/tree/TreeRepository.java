package de.ship.clip.lib.tree;

import de.ship.dbppship.provider.db.tables.TCoreElementHierarchy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static de.ship.dbppship.provider.db.Tables.*;

public abstract class TreeRepository {
	private static final Logger LOGGER = LogManager.getLogger(TreeRepository.class);

	public ElementBean[] getChildElements(DSLContext dslContext, Integer elementId) {
		TCoreElementHierarchy E = T_CORE_ELEMENT_HIERARCHY.as("element");
		TCoreElementHierarchy child = T_CORE_ELEMENT_HIERARCHY.as("child");
		Field<Boolean> has_children = DSL.field("has_children", Boolean.class);
		// @formatter:off
		SelectSeekStep1<Record5<Integer,Integer,String,String,Boolean>,Integer> sql = dslContext
			.select(
					E.KEY_ELEMENT,
					E.KEY_PARENT_ELEMENT,
					E.UNIQUE_NAME,
					E.SHORT_NAME,
					DSL.coalesce(has_children, false).as(has_children)
				)
			.from(E)
			.leftJoin(
					dslContext.select(
								DSL.count(E.KEY_PARENT_ELEMENT).ge(0).as(has_children)
								, E.KEY_PARENT_ELEMENT)
						.from(E)
						.groupBy(E.KEY_PARENT_ELEMENT).asTable(child)
			).on(E.KEY_ELEMENT.eq(child.KEY_PARENT_ELEMENT))
			.where(elementId == null ? E.KEY_PARENT_ELEMENT.isNull() : E.KEY_PARENT_ELEMENT.eq(elementId))
			.orderBy(E.ORDER_NR);
		// @formatter:on
		LOGGER.trace(sql.toString());

		List<ElementBean> list = new ArrayList<>();

		for (Record5<Integer, Integer, String, String, Boolean> r : sql.fetch()) {
			String shortName = r.get(E.SHORT_NAME);
			String uniqueName = r.get(E.UNIQUE_NAME);
			Integer key = r.get(E.KEY_ELEMENT, Integer.class);
			Integer keyParentElement = r.get(E.KEY_PARENT_ELEMENT);
			Boolean hasChild = r.get(has_children);
			list.add(ElementBean.of(key, keyParentElement, shortName, uniqueName, hasChild));
		}
		return list.toArray(new ElementBean[list.size()]);
	}

	public ElementBean[] getFilteredElements(DSLContext dslContext, String filter, boolean onlyVars) {
		List<ElementBean> list = new ArrayList<>();

		TCoreElementHierarchy E = T_CORE_ELEMENT_HIERARCHY.as("element");
		TCoreElementHierarchy child = T_CORE_ELEMENT_HIERARCHY.as("child");
		Field<Boolean> has_children = DSL.field("has_children", Boolean.class);
		// @formatter:off
		SelectOnConditionStep<Record5<Integer, Integer, String, String, Boolean>> queryWithOnlyVars;
		SelectSeekStep1<Record5<Integer, Integer, String, String, Boolean>, Integer> finalQuery;
		SelectOnConditionStep<Record5<Integer,Integer,String,String,Boolean>> initialQuery = dslContext
			.select(
					E.KEY_ELEMENT,
					E.KEY_PARENT_ELEMENT,
					E.SHORT_NAME,
					E.UNIQUE_NAME,
					DSL.coalesce(has_children, false).as(has_children)
				)
			.from(E)
			.leftJoin(
					dslContext.select(
						DSL.count(E.KEY_PARENT_ELEMENT).ge(0).as(has_children)
						, E.KEY_PARENT_ELEMENT)
					.from(E)
					.groupBy(E.KEY_PARENT_ELEMENT).asTable(child)
			).on(E.KEY_ELEMENT.eq(child.KEY_PARENT_ELEMENT));

		if (onlyVars) {
			queryWithOnlyVars = initialQuery.innerJoin(T_CORE_VARIABLE).on(E.KEY_ELEMENT.eq(T_CORE_VARIABLE.KEY_VARIABLE));
			finalQuery = queryWithOnlyVars.where(E.SHORT_NAME.likeIgnoreCase("%"+filter+"%")).orderBy(E.ORDER_NR);
		} else {
			finalQuery = initialQuery.where(E.SHORT_NAME.likeIgnoreCase("%"+filter+"%")).orderBy(E.ORDER_NR);
		}

		// @formatter:on
		LOGGER.trace(finalQuery.toString());

		for (Record5<Integer, Integer, String, String, Boolean> r : finalQuery.fetch()) {
			String shortName = r.get(E.SHORT_NAME);
			String uniqueName = r.get(E.UNIQUE_NAME);
			Integer key = r.get(E.KEY_ELEMENT, Integer.class);
			Integer keyParentElement = r.get(E.KEY_PARENT_ELEMENT);
			Boolean hasChild = r.get(has_children);
			list.add(ElementBean.of(key, keyParentElement, shortName, uniqueName, hasChild));
		}
		return list.toArray(new ElementBean[list.size()]);
	}

	public Integer[] getPath(DSLContext dslContext, Integer keyElement) {
		TCoreElementHierarchy RECURSIVE_ELEMENTS = T_CORE_ELEMENT_HIERARCHY.as("RECURSIVE_ELEMENTS");
		TCoreElementHierarchy PARENT_ELEMENTS = T_CORE_ELEMENT_HIERARCHY.as("PARENT_ELEMENTS");
		TCoreElementHierarchy START_ELEMENT = T_CORE_ELEMENT_HIERARCHY.as("START_ELEMENT");

		// @formatter:off
		SelectJoinStep<Record2<Integer, Integer>> sql = dslContext.withRecursive(RECURSIVE_ELEMENTS.getName())
			.as(
				DSL.select(
					START_ELEMENT.KEY_ELEMENT,
					START_ELEMENT.KEY_PARENT_ELEMENT
				).from(START_ELEMENT)
				.where(START_ELEMENT.KEY_ELEMENT.eq(keyElement))
				.unionAll(DSL.select(
					PARENT_ELEMENTS.KEY_ELEMENT,
					PARENT_ELEMENTS.KEY_PARENT_ELEMENT
				).from(PARENT_ELEMENTS)
				.join(DSL.table(DSL.name(RECURSIVE_ELEMENTS.getName()))).on(PARENT_ELEMENTS.KEY_ELEMENT.equal(RECURSIVE_ELEMENTS.KEY_PARENT_ELEMENT))))
			.select(
				RECURSIVE_ELEMENTS.KEY_ELEMENT,
				RECURSIVE_ELEMENTS.KEY_PARENT_ELEMENT
			).from(DSL.table(DSL.name(RECURSIVE_ELEMENTS.getName())));
		// @formatter:on

		LOGGER.trace(sql.toString());

		List<Integer> list = new ArrayList<>();
		for (Record2<Integer, Integer> r : sql.fetch()) {
			list.add(r.get(RECURSIVE_ELEMENTS.KEY_ELEMENT));
		}

		// reverse the list to get the path from root to element
		Collections.reverse(list);

		return list.toArray(new Integer[list.size()]);
	}
}