package de.ship.clip.lib.menu;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.gson.Gson;

/**
 * 
 * @author henkej
 *
 */
public class TestListOfMenuitemBeansWrapperConverter {

	@Test
	public void testToGson() {
		List<MenuitemBean> menuitems = new ArrayList<>();
		menuitems.add(MenuitemBean.of("myAwesomeApp1").withUrl("https://my.awesome.url")
				.withLabel(DisplayLabel.ofTranslation("a.common.name")).withDescription(DisplayLabel.ofName("a description")).withIconUrl("an ICON url"));
		menuitems.add(MenuitemBean.of("myAwesomeApp2").withUrl("https://another.awesome.url")
				.withLabel(DisplayLabel.ofName("another name"))
				.withDescription(DisplayLabel.ofTranslation("a.common.description")).withIconUrl("another ICON url").withCssClass("myCssClass"));
		ListOfMenuitemBeansWrapper w = ListOfMenuitemBeansWrapper.of(menuitems);
		String json = new Gson().toJson(w);
		System.out.println(json);
		assertNotNull(json);
		assertTrue(json.contains("\"version\":3"));
	}
}
