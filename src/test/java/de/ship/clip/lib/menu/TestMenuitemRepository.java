package de.ship.clip.lib.menu;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestMenuitemRepository {

	@Test
	public void testLoadFromMenuUrl() {
		MenuitemRepository repository = new MenuitemRepository() {
		};
		ListOfMenuitemBeansWrapper wrapper = repository.loadMenuFromUrl("http://localhost:9001/Shipclip/menu");
		assertNotNull(wrapper);
	}
}
