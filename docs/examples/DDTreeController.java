import de.ship.clip.lib.tree.ElementBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ddtree")
public class DDTreeController {
	
	@Autowired
	private DDTreeService service;

	@GetMapping("/children/{keyElement}")
	public ElementBean[] getChildElements(@PathVariable("keyElement") Integer keyElement) {
		return service.getChildElements(keyElement);
	}

	@GetMapping("/children")
	public ElementBean[] getRootElements() {
		return service.getRootElements();
	}

	@GetMapping("/filter/{query}")
	public ElementBean[] getFilteredElements(@PathVariable("query") String query, @RequestParam(required = false, value = "onlyVars") Boolean onlyVars) {
		if (onlyVars == null) {
			onlyVars = false;
		}
		return service.getFilteredElements(query, onlyVars);
	}

	@GetMapping("/path/{keyElement}")
	public Integer[] getPath(@PathVariable("keyElement") Integer keyElement) {
		return service.getPath(keyElement);
	}
}
