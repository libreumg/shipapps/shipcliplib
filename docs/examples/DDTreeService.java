import de.ship.clip.lib.tree.ElementBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DDTreeService {


	@Autowired
	private DDTreeRepository ddTreeRepository;
	/**
	 * get children of the given element
	 * 
	 * @param keyElement the key of the element
	 * @return the children; an empty array at least
	 */
	public ElementBean[] getChildElements(Integer keyElement) {
		return ddTreeRepository.getChildElements(keyElement);
	}

	/**
	 * get root elements
	 * 
	 * @return the root elements; an empty array at least
	 */
	public ElementBean[] getRootElements() {
		return ddTreeRepository.getChildElements(null);
	}


	/**
	 * get all variables filtered by a string
	 * 
	 * @param filter the filter string
	 * @param onlyVars if true only variables are returned, otherwise all elements
	 * @return the variables; an empty array at least
	 */
	public ElementBean[] getFilteredElements(String filter, boolean onlyVars) {
		return ddTreeRepository.getFilteredElements(filter, onlyVars);
	}



	/**
	 * get the path of the given element
	 * 
	 * @param keyElement the key of the element
	 * @return the path; an empty array at least if the element is not found
	 */
	public Integer[] getPath(Integer keyElement) {
		return ddTreeRepository.getPath(keyElement);
	}
}