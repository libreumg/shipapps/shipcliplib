import de.ship.clip.lib.tree.ElementBean;
import de.ship.clip.lib.tree.TreeRepository;
import org.jooq.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DDTreeRepository extends TreeRepository {

    @Autowired
    private DSLContext dslContext;


	public ElementBean[] getChildElements(Integer elementId) {
		return super.getChildElements(dslContext, elementId);
	}

	public ElementBean[] getFilteredElements( String filter, boolean onlyVars) {
		return super.getFilteredElements(dslContext, filter, onlyVars);
	}

	public Integer[] getPath(Integer keyElement) {
		return super.getPath(dslContext, keyElement);
	}
}