The library makes it possible to use a consistent menu experience across different web applications without feeling like you're leaving the application. Boundaries can also be crossed across different servers, and the domain can change as well.

Basically, this is achieved by providing the menu bar, through which already integrated basic functionalities such as language change or profile settings of Keycloak.

see locales.png

The menu item `modules` on the left can then be used to easily switch between registered applications.

see modules.png
