The following describes how to set up a Spring Boot application using Thymeleaf to be compatible with Shipclip.

# setup of the basic files
## the main application
Shipclip compatible applications are web applications that can also be deployed as war for a tomcat.
```java
@SpringBootApplication
public class MyFamousApp extends SpringBootServletInitializer {

  @Override
  protected SpringApplicationBuilder configure(
    SpringApplicationBuilder application) {
    return application.sources(MyFamousApp.class);
  }
  
	public static void main(String[] args) {
		SpringApplication.run(MyFamousApp.class, args);
	}
}
```

## the menu
The menu provided via a REST endpoint is determined from Shipclip. For this purpose, a repository can be written that inherits from `en.ship.clip.lib.menu.MenuitemRepository` and uses the `loadMenuFromUrl` method.
```java
@Repository
public class MenuRepository extends MenuitemRepository {
	
	@Value("${external.shipclip.menu.url}")
	private String menuUrl;
	
	public List<MenuitemBean> getMenuitems() {
		ListOfMenuitemBeansWrapper wrapper = loadMenuFromUrl(menuUrl);
		return wrapper.getList();
	}
}
```
The menu items provided by this can be included by an abstract controller for all controllers of the application by inheritance. In the following code example, the application that is currently running is excluded.
```java
public abstract class MenuController {

	@Autowired
	private MenuRepository repository;
	
	@Autowired
	private KeycloakInformation keycloakInformation;
	
	@Value("${server.servlet.context-path}")
	private String appId;
	
	/**
	 * get the check bean for the realm role check
	 * 
	 * @return the check bean
	 */
	@ModelAttribute("check")
	public CheckBean getCheckBean() {
		return new CheckBean(keycloakInformation);
	}

	/**
	 * get the menu items
	 * 
	 * @return the menu items
	 */
	@ModelAttribute("menuitems")
	public List<MenuitemBean> getMenuitems() {
		List<MenuitemBean> list = getItems(LocaleContextHolder.getLocale());
		for (MenuitemBean bean : list) {
			if (bean.getAppId().equals(appId.replace("/", ""))) {
				bean.setCssClass(MenuitemBean.SELECTED_CLASS);
			}
		}
		return list;
	}
}
```

### the user profile
#### keycloak support
To provide the user profile in Keycloak, the method `getKeycloakUserUrl` is needed. This can be inherited and simplified from `en.ship.clip.lib.common.KeycloakInformationProvider`.
```java
@Component
public class KeycloakInformation extends KeycloakInformationProvider {

	@Autowired
	private HttpServletRequest request;

	@Value("${keycloak.resource}")
	private String keycloakClientId;

	@Value("${keycloak.auth-server-url}")
	private String keycloakUrl;

	@Value("${keycloak.realm:ship}")
	private String keycloakRealm;

	@Override
	public String getKeycloakUserUrl() {
		return super.getKeycloakUserUrl(keycloakClientId, keycloakUrl, keycloakRealm);
	}
		
	@Override
	public HttpServletRequest getRequest() {
		return request;
	}
}
```

### the version number
#### build information
For a simplified representation of the BuildInformation, the class `de.ship.clip.lib.common.BuildInformationProvider` can be inherited, which provides the two methods `getContextPath` and `getVersion`.

```java
@Component
public class BuildInformation extends BuildInformationProvider {
	@Value("${server.servlet.context-path:}")
	private String contextPath;

	@Autowired(required = false)
	private BuildProperties buildProperties;

	@Override
	public String getContextPath() {
		return getContextPath(contextPath);
	}

	@Override
	public String getVersion() {
		return getVersion(buildProperties);
	}
}
```

### the language support
#### enabling the switch of languages
To change the language, the menu provides a `LocaleChangeInterceptor`. This must be known and can be included in any `Configuration` via the class `en.ship.clip.lib.common.ConfigurationHelper`. Also, the locale resolver bean enables locale changes by this configuration.

```java
@Configuration
public class WebConfiguration implements WebMvcConfigurer {
  @Bean
  public LocaleChangeInterceptor localeChangeInterceptor() {
    return ConfigurationHelper.getLocaleChangeInterceptor();
  }

	@Bean
	public LocaleResolver localeResolver() {
		final SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(Locale.GERMAN);
		return localeResolver;
	}

  @Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
	}
}
```
#### adding predefined messages for the menu
The menu already uses language keys, which can be easily made known to the whole application by passing the `basename` to the `ReloadableResourceBundleMessageSource`. The order of the sources is taken into account.

The class `en.ship.clip.lib.common.ConfigurationHelper` provides a constant `LIB_MESSAGES` for this purpose.
```java
@Configuration
public class WebConfiguration implements WebMvcConfigurer {
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames(ConfigurationHelper.LIB_MESSAGES, "classpath:/messages/messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}
}
```

## the tree
The tree is a component that can be used to display a tree structure. It can be embedded in any page by using the TreeView.js script. The script fetches the tree structure from a REST endpoint and displays it in a tree structure. 
The following example shows how to use it in a Thymeleaf template.
```html 
<div class="d-flex mb-2">
	<select id="jstree-filter-select" placeholder="Filter" class="form-select"></select>
	<button id="jstree-collapse-btn" type="button" class="btn btn-outline-primary mx-2">
		<i class="fa-solid fa-down-left-and-up-right-to-center"></i>
	</button>
	<button id="jstree-refresh-btn" type="button" class="btn btn-outline-primary">
		<i class="fa-solid fa-rotate-right"></i>
	</button>
</div>
<div id="tree-path" class="d-flex">
	<div th:text="#{treeview.pathtext}"></div>
	<div id="element-path" class="ms-2">/</div>
</div>
<div id="jstree"></div>
<script type="text/javascript" >
	$(document).ready(function() {
		const treeView = new TreeView({
				treeId: "jstree",
				backendApiUrl: '[[${#request.contextPath}]]' + "/ddtree",
				nodeIconClass: 'fas fa-folder-closed tree-icon-node',
				leafIconClass: 'far fa-file-lines tree-icon-leaf',
				treeFilterId: "jstree-filter-select",					// optional
				collapseBtnId: "jstree-collapse-btn",					// optional
				treePathId: "element-path",								// optional
				refreshBtnId: "jstree-refresh-btn",						// optional
			}
		);
		treeView.init();						
	});
</script>
```
Please take a look at the [TreeView-API]() for more information.

For the REST endpoint, a controller has to be implemented that accesses a repository which inherits from `en.ship.clip.lib.tree.TreeRepository` via a service. 
For examples, take a look at the following files:
- [DDTreeController.java](./examples/DDTreeController.java)
- [DDTreeRepository.java](./examples/DDTreeRepository.java)
- [DDTreeService.java](./examples/DDTreeService.java)

### dev usage
If you develope an app that uses the tree component and you want to make changes to the tree, you can use the `./gradlew build publishToMavenLocal` command to publish the library to your local maven repository. 
To use the local version of the library, you have to rebuild your app.

## Resources
### properties
In the `application.properties`, the keys `extern.shipclip.menu.url` and `server.servlet.context-path` must be provided. Also, if using keycloak, those settings must be defined as usual.

### template
For all pages, a template should be used. The definition of such on can use fragments of the library shipcliplib, which offers the header fragment for the menu bar and the messages fragment for errors, warnings or infos. As the menu bar needs `bootstrap`, `font-awesome` and `jquery`, the following example can be used.
```xml
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:th="http://www.thymeleaf.org" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" xmlns:sec="http://www.thymeleaf.org/extras/spring-security">
<head>
<meta charset="UTF-8" />
<title>MyAwesomeApp</title>
<link rel="stylesheet" type="text/css" th:href="@{/webjars/bootstrap/5.2.3/css/bootstrap.min.css}" />
<link rel="stylesheet" type="text/css" th:href="@{/webjars/font-awesome/6.3.0/css/all.min.css}" />
<link rel="stylesheet" type="text/css" th:href="@{/css/style.css}" />
<link rel="icon" type="image/png" sizes="32x32" th:href="@{/images/favicon/favicon-32x32.png}" />
<link rel="icon" type="image/png" sizes="16x16" th:href="@{/images/favicon/favicon-16x16.png}" />
<script type="application/javascript" th:src="@{/webjars/jquery/3.6.3/jquery.min.js}"></script>
<script type="application/javascript" th:src="@{/webjars/popper.js/2.9.3/umd/popper.min.js}"></script>
<script type="application/javascript" th:src="@{/webjars/bootstrap/5.2.3/js/bootstrap.bundle.min.js}"></script>

<!-- Optional dependencies for the tree component -->
<link rel="stylesheet" type="text/css" th:href="@{/webjars/jstree/3.3.14/themes/default/style.css}" />
<script type="application/javascript" th:src="@{/webjars/jstree/3.3.14/jstree.js}"></script>
<script type="application/javascript" th:src="@{/js/TreeView.js}"></script>
<link rel="stylesheet" type="text/css" th:href="@{/webjars/select2/4.0.13/css/select2.min.css}" />
<link rel="stylesheet" type="text/css" th:href="@{/css/select2-bootstrap-5-theme.min.css}" />
<script th:src="@{/webjars/select2/4.0.13/js/select2.full.min.js}"></script>

</head>
<body class="page-bs">
	<div id="header" class="header-bs">
		<div th:replace="fragments/header::topNavigationBarBS()"></div>
	</div>
	<div id="wrapper">
		<div id="main" class="medium container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div th:replace="fragments/messages::messages"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<th:block layout:fragment="content">
					</th:block>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
```
Also, a `menu.html` is expected in the main folder:
```xml
<html xmlns:th="http://www.thymeleaf.org">
<th:block th:fragment="menu()">
	<li class="nav-item"><a class="nav-link" th:href="@{/anyDestination}">example 1</a></li>
	<li class="nav-item"><a class="nav-link" th:href="@{/aotherDestination}">example 2</a></li>
</th:block>
</html>
```

